# Build stage
FROM golang:1.23.0-alpine AS builder

# Copy files
COPY ./pkg prometheus-adapter/pkg
COPY ./cmd prometheus-adapter/cmd
COPY ./go.mod prometheus-adapter/go.mod
COPY ./go.sum prometheus-adapter/go.sum

# Install dependencies and build the project
RUN apk add --no-cache git bash \
    && cd prometheus-adapter \
    && go mod download \
    && CGO_ENABLED=0 go build -a --ldflags '-w' -o /go/prometheus-gaussdb-adapter ./cmd/prometheus-gaussdb-adapter

# Final image
FROM alpine:latest
MAINTAINER JiaXinYang
COPY --from=builder /go/prometheus-gaussdb-adapter /
ENTRYPOINT ["/prometheus-gaussdb-adapter"]
